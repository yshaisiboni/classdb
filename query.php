<?php
    class Query{
        protected $dbc;
        function __construct($dbc){
            $this->dbc = $dbc;
        }

        public function query($query){
            if($result = $this->dbc->query($query)){ //הצליח
                return $result;                
            }
            else { //נכשל
                echo "Something went wrong with the query";
            }
        }
    }
?>